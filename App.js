import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';

import { Provider, useSelector, } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from './src/store';

import Navigator from './src/navigator';

import SplashScreen from 'react-native-splash-screen';


LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])
    
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <NavigationContainer>
                    <StatusBar backgroundColor = "#0D0D0D"   />  
                    <Navigator />
                </NavigationContainer>
            </PersistGate>
        </Provider>
    )
}

export default App


// import React, { useCallback, useEffect, useState } from 'react';
// import {
//   StyleSheet,
//   Text,
//   TextInput,
//   TouchableOpacity,
//   useColorScheme,
//   View,
// } from 'react-native';

// import { get, save } from './src/utils/storage';
// import { Colors } from './src/global';



// const HomeScreen = () => {
//   const [themeValue, setThemeValue] = useState('');
//   const [initialValue, setInitialValue] = useState(0);
//   const themes = useColorScheme();
//   const data = [
//     {
//       label: 'Light Mode',
//       value: 'light',
//     },
//     {
//       label: 'Dark Mode',
//       value: 'dark',
//     },
//     {
//       label: 'System Default',
//       value: 'default',
//     },
//   ];

//   const themeOperations = theme => {
//     switch (theme) {
//       case 'dark':
//         setTheme(theme, false);
//         setInitialValue(2);
//         return;
//       case 'light':
//         setTheme(theme, false);
//         setInitialValue(1);
//         return;
//       case 'default':
//         setTheme(themes, true);
//         setInitialValue(3);
//         return;
//     }
//   };

//   const getAppTheme = useCallback(async () => {
//     const theme = await get('Theme');
//     const isDefault = await get('IsDefault');
//     isDefault ? themeOperations('default') : themeOperations(theme);
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, []);

//   const setTheme = useCallback(async (theme, isDefault) => {
//     save('Theme', theme);
//     save('IsDefault', isDefault);
//     setThemeValue(theme);
//   }, []);

//   useEffect(() => {
//     getAppTheme();
//   }, [getAppTheme]);

//   const styles = styling(themeValue);

//   return (
//     <View style={styles.container}>
//       <Text style={styles.textStyle}>
//         This is demo of default dark/light theme with switch/Buttons using asycn
//         storage.
//       </Text>
//       <TextInput
//         style={styles.textInputStyle}
//         placeholder="Type here"
//         placeholderTextColor={Colors[themeValue]?.gray}
//       />
//       <TouchableOpacity 
//         onPress={() => {themeOperations('light')}}
//         style={styles.touchableStyle}>
//         <Text style={styles.buttonTextStyle}>LIGHT</Text>
//       </TouchableOpacity>

//       <TouchableOpacity 
//         onPress={() => {themeOperations('dark')}}
//         style={styles.touchableStyle}>
//         <Text style={styles.buttonTextStyle}>DARK</Text>
//       </TouchableOpacity>

//       <TouchableOpacity 
//         onPress={() => {themeOperations('default')}}
//         style={styles.touchableStyle}>
//         <Text style={styles.buttonTextStyle}>SYSTEM</Text>
//       </TouchableOpacity>
//     </View>
//   );
// };

// export default HomeScreen;

// const styling = theme =>
//   StyleSheet.create({
//     container: {
//       flex: 1,
//       justifyContent: 'center',
//       backgroundColor: Colors[theme]?.themeColor,
//       paddingHorizontal: 20,
//     },
//     textStyle: {
//       color: Colors[theme]?.white,
//     },
//     textInputStyle: {
//       borderColor: Colors[theme]?.gray,
//       padding: 10,
//       borderWidth: 2,
//       borderRadius: 5,
//       width: '100%',
//       marginTop: 20,
//       color: Colors[theme]?.white,
//     },
//     touchableStyle: {
//       backgroundColor: Colors[theme]?.sky,
//       padding: 10,
//       borderRadius: 6,
//       width: '100%',
//       height: 57,
//       justifyContent: 'center',
//       marginTop: 20,
//     },
//     buttonTextStyle: {
//       textAlign: 'center',
//       color: Colors[theme]?.commonWhite,
//       fontSize: 20,
//       fontWeight: '500',
//     },
//   });

