import { colors } from "./colors";
import { media } from "./media";


export const dummyCategories = [
    {
        "categoryName": 'Medical',
        "categoryDescription": 'Doctors bill, medical bills, etc.',
        "categoryIcon": {"title": "heartbeat", "type": "FontAwesome"},
        "categoryColor": {
            "iconColor": '#520214',
            "bgColor": '#ddcccf',
        },
      "id": 1
    },
    {
        "categoryName": 'Travelling',
        "categoryDescription": 'Bus tickets, Train tickets, fuel bill, etc.',
        "categoryIcon": {"title": "parking", "type": "MaterialCommunityIcons"},
        "categoryColor": {
            "iconColor": '#b24272',
            "bgColor": '#f2d0de',
        },
      "id": 2
    },
    {
        "categoryName": 'Bills and Utilities',
        "categoryDescription": 'Gas, electricity, water, internet bill, etc.',
        "categoryIcon": {"title": "text-document-inverted", "type": "Entypo"},
        "categoryColor": {
            "iconColor": '#ec886b',
            "bgColor": '#fce4dc',
        },
      "id": 3
    }
  ]

export const dummyExpenses = [
    {
      "amount": "200",
      "categoryDescription": "Shopping item",
      "categoryName": "Shopping",
      "currentCategory": {
        "categoryName": 'Shopping',
        "categoryDescription": 'Apparels shopping, Appliances shopping',
        "categoryIcon": {"title": "shopping-cart", "type": "Entypo"},
        "categoryColor": {
            "iconColor": '#4051b6',
            "bgColor": '#d9dbf1',
        },
      },
      "dateValue": "Mon Sep 18 2023 22:38:43 GMT+0530",
      "id": 1
    },
    {
      "amount": "550",
      "categoryDescription": "Bills electricity ",
      "categoryName": "Bills and Utilities",
      "currentCategory": {
        "categoryName": 'Bills and Utilities',
        "categoryDescription": 'Gas, electricity, water, internet bill, etc.',
        "categoryIcon": {"title": "text-document-inverted", "type": "Entypo"},
        "categoryColor": {
            "iconColor": '#ec886b',
            "bgColor": '#fce4dc',
        },
        "id": 3
      },
      "dateValue": "2023-09-17T17:11:00.000Z",
      "id": 3
    },
    {
      "amount": "688",
      "categoryDescription": "Other items",
      "categoryName": "Others",
      "currentCategory": {
        "categoryName": 'Others',
        "categoryDescription": 'Miscellaneous expenses',
        "categoryIcon": {"title": "dots-three-horizontal", "type": "Entypo"},
        "categoryColor": {
            "iconColor": '#015555',
            "bgColor": '#ADDCCC',
        },
      },
      "dateValue": "Mon Sep 18 2023 22:45:40 GMT+0530",
      "id": 1
    },
    {
      "amount": "898",
      "categoryDescription": "Cbb",
      "categoryName": "Travelling",
      "currentCategory": {
        "categoryName": 'Travelling',
        "categoryDescription": 'Bus tickets, Train tickets, fuel bill, etc.',
        "categoryIcon": {"title": "parking", "type": "MaterialCommunityIcons"},
        "categoryColor": {
            "iconColor": '#b24272',
            "bgColor": '#f2d0de',
        },
        "id": 2
      },
      "dateValue": "Mon Sep 18 2023 22:46:11 GMT+0530",
      "id": 2
    },
    {
      "amount": "898",
      "categoryDescription": "Cvc",
      "categoryName": "Medical ",
      "currentCategory": {
        "categoryName": 'Medical',
        "categoryDescription": 'Doctors bill, medical bills, etc.',
        "categoryIcon": {"title": "heartbeat", "type": "FontAwesome"},
        "categoryColor": {
            "iconColor": '#520214',
            "bgColor": '#ddcccf',
        },
        "id": 1
      },
      "dateValue": "Mon Sep 18 2023 22:47:04 GMT+0530",
      "id": 3
    }
  ]


export const categories = [
    {
        categoryName: 'Others',
        categoryDescription: 'Miscellaneous expenses',
        categoryIcon: {"title": "dots-three-horizontal", "type": "Entypo"},
        categoryColor: {
            iconColor: '#015555',
            bgColor: '#ADDCCC',
        },
    },
    {
        categoryName: 'Food and Dining',
        categoryDescription: 'Groceries, dairy products, restaurant bill',
        categoryIcon: {"title": "food", "type": "MaterialCommunityIcons"},
        categoryColor: {
            iconColor: '#e25bc2',
            bgColor: '#fad6f3',
        },
    },
    {
        categoryName: 'Shopping',
        categoryDescription: 'Apparels shopping, Appliances shopping',
        categoryIcon: {"title": "shopping-cart", "type": "Entypo"},
        categoryColor: {
            iconColor: '#4051b6',
            bgColor: '#d9dbf1',
        },
    },
    {
        categoryName: 'Travelling',
        categoryDescription: 'Bus tickets, Train tickets, fuel bill, etc.',
        categoryIcon: {"title": "parking", "type": "MaterialCommunityIcons"},
        categoryColor: {
            iconColor: '#b24272',
            bgColor: '#f2d0de',
        },
    },
    {
        categoryName: 'Entertainment',
        categoryDescription: 'Movie tickets, amusement park, rides',
        categoryIcon: {"title": "game-controller", "type": "Entypo"},
        categoryColor: {
            iconColor: '#34897f',
            bgColor: '#cfe2e2', 
        },
    },
    {
        categoryName: 'Medical',
        categoryDescription: 'Doctors bill, medical bills, etc.',
        categoryIcon: {"title": "heartbeat", "type": "FontAwesome"},
        categoryColor: {
            iconColor: '#520214',
            bgColor: '#ddcccf',
        },
    },
    {
        categoryName: 'Personal Care',
        categoryDescription: 'Skin care products, hair products, cosmetics',
        categoryIcon: {"title": "bath", "type": "FontAwesome5"},
        categoryColor:     {
            iconColor: '#94bc6a',
            bgColor: '#e5f1d8',
        },
    },
    {
        categoryName: 'Education',
        categoryDescription: 'Tution fees, scholol fees, etc.',
        categoryIcon: {"title": "graduation-cap", "type": "FontAwesome"},
        categoryColor: {
            iconColor: '#681b9a',
            bgColor: '#e2d2eb',
        },
    },
    {
        categoryName: 'Bills and Utilities',
        categoryDescription: 'Gas, electricity, water, internet bill, etc.',
        categoryIcon: {"title": "text-document-inverted", "type": "Entypo"},
        categoryColor: {
            iconColor: '#ec886b',
            bgColor: '#fce4dc',
        },
    },
    {
        categoryName: 'Gifts and Donation',
        categoryDescription: 'Gifts, donation, etc.',
        categoryIcon: {"title": "gift", "type": "FontAwesome5"},
        categoryColor:  {
            iconColor: '#4051b6',
            bgColor: '#d9dbf1',
        },
    },
]

export const colors_array = [
    {
        iconColor: '#dc615c',
        bgColor: '#f9d7d7',
    },
    {
        iconColor: '#34897f',
        bgColor: '#cfe2e2', 
    },
    {
        iconColor: '#b24272',
        bgColor: '#f2d0de',
    },
    {
        iconColor: '#e25bc2',
        bgColor: '#fad6f3',
    },
    {
        iconColor: '#520214',
        bgColor: '#ddcccf',
    },
    {
        iconColor: '#94bc6a',
        bgColor: '#e5f1d8',
    },
    {
        iconColor: '#2c4e4e',
        bgColor: '#d5ddde',
    },
    {
        iconColor: '#2e2e2e',
        bgColor: '#d6d6d6',
    },
    {
        iconColor: '#4051b6',
        bgColor: '#d9dbf1',
    },
    {
        iconColor: '#ae1456',
        bgColor: '#efd1dc',
    },
    {
        iconColor: '#b2ece5',
        bgColor: '#dcf4f2',
    },
    {
        iconColor: '#003e01',
        bgColor: '#ccd9cc',
    },
    {
        iconColor: '#aa5b74',
        bgColor: '#f2dbe3',
    },
    {
        iconColor: '#ec886b',
        bgColor: '#fce4dc',
    },
    {
        iconColor: '#681b9a',
        bgColor: '#e2d2eb',
    },
    
]

export const food_icon_array = [
    {
        'title': 'food', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-apple', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-croissant', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-drumstick', 
        'type': 'MaterialCommunityIcons'
    },{
        'title': 'food-fork-drink', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-hot-dog', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-outline', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-steak', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-takeout-box', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-turkey', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'food-variant', 
        'type': 'MaterialCommunityIcons'
    },
]

export const travel_icon_array = [
    {
        'title': 'map-marker-path', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'fuel', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'parking', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'car', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'bike', 
        'type': 'MaterialCommunityIcons'
    },
]

export const shopping_icon_array = [
    {
        'title': 'shopping-cart', 
        'type': 'Entypo'
    },
    {
        'title': 'price-tag', 
        'type': 'Entypo'
    },
    {
        'title': 'shopify', 
        'type': 'Fontisto'
    },
    {
        'title': 'tshirt', 
        'type': 'FontAwesome5'
    },
    {
        'title': 'shoe-sneaker', 
        'type': 'MaterialCommunityIcons'
    },
    {
        'title': 'sofa-single', 
        'type': 'MaterialCommunityIcons'
    },

    
]

export const Entertainment_icon_array = [
     {
        'title': 'game-controller', 
        'type': 'Entypo'
    },
    {
        'title': 'video', 
        'type': 'Entypo'
    },
    {
        'title': 'headphone', 
        'type': 'Fontisto'
    },
    {
        'title': 'netflix', 
        'type': 'MaterialCommunityIcons'
    },
]



export const Miscellaneous_icon_array = [
     {
        'title': 'bath', 
        'type': 'FontAwesome5'
    },
    {
        'title': 'graduation-cap', 
        'type': 'FontAwesome'
    },
    {
        'title': 'heartbeat', 
        'type': 'FontAwesome'
    },
    {
        'title': 'gift', 
        'type': 'FontAwesome5'
    },
    {
        'title': 'text-document-inverted', 
        'type': 'Entypo'
    },
    {
        'title': 'dots-three-horizontal', 
        'type': 'Entypo'
    },

    
]

