const commonColor = {
  commonWhite: '#FFFFFF',
  commonBlack: '#000000',
  activeColor: '#DE5E69',
  deactiveColor: '#DE5E6950',
  boxActiveColor: '#DE5E6940',
  primaryColor: '#015555',
  primaryLightColor: '#ADDCCC',
  grayColor: '#949494',
  greenColor: '#015555',
  lightRedColor: '#FFE2E4',
  redColor: '#e3242b',
};

const light = {
  themeColor: '#FFFFFF',
  themeColorOpac: '#00000050',
  white: '#000000',
  sky: '#DE5E69',
  gray: '#f5f5f5',
  ...commonColor,
};

const dark = {
  themeColor: '#000000',
  themeColorOpac: '#ffffff50',
  white: '#FFFFFF',
  sky: '#831a23',
  gray: '#202020',
  ...commonColor,
};

export default { light, dark };