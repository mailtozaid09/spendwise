export const media = {
    
    user_img: require('../../assets/images/user_img.jpeg'),

    logo: require('../../assets/images/logo.jpeg'),
    welcome: require('../../assets/images/welcome.png'),

    
    home: require('../../assets/icons/home.png'),
    home_active: require('../../assets/icons/home_active.png'),

    summary: require('../../assets/icons/summary.png'),
    summary_active: require('../../assets/icons/summary_active.png'),
    
    category: require('../../assets/icons/category.png'),
    category_active: require('../../assets/icons/category_active.png'),

    user: require('../../assets/icons/user.png'),
    user_active: require('../../assets/icons/user_active.png'),


    dark_mode: require('../../assets/icons/dark-mode.png'),
    settings: require('../../assets/icons/settings.png'),
    account: require('../../assets/icons/account.png'),
    upload: require('../../assets/icons/upload.png'),
    edit: require('../../assets/icons/edit.png'),
    logout: require('../../assets/icons/logout.png'),


    dark_mode_lottie: require('../../assets/lottie/dark_mode_lottie.json'),
    empty_lottie: require('../../assets/lottie/empty_lottie.json'),
    edit_lottie: require('../../assets/lottie/edit_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    error_lottie: require('../../assets/lottie/error_lottie.json'),
    logout_lottie: require('../../assets/lottie/logout_lottie.json'),
    notification_lottie: require('../../assets/lottie/notification_lottie.json'),
    success_lottie: require('../../assets/lottie/success_lottie.json'),


}
