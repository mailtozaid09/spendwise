
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer } from 'redux-persist';

export default reducers => {
  const persistedReducer = persistReducer(
    {
      key: 'ilegra',
      storage: AsyncStorage,
      whitelist: ['auth', 'home', 'profile'],
    },
    reducers
  );

  return persistedReducer;
};
