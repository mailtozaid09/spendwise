import { dummyCategories, dummyExpenses } from "../../../global/sampleData";
import { ADD_CATEGORY_ITEM, ADD_EXPENSE_DETAILS, 
    DELETE_CATEGORY_ITEM, DELETE_EXPENSE_DETAILS, 
    EDIT_CATEGORY_ITEM, EDIT_EXPENSE_DETAILS 
} from "./actionTypes";

const INITIAL_STATE = {
    screen_loader: false,
    category_list: dummyCategories,
    expense_list: dummyExpenses,
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {

    case ADD_CATEGORY_ITEM: {
        const {
            id, categoryName, categoryDescription, categoryIcon, categoryColor
        } = action.payload
        return {
            ...state,
            category_list: [
                ...state.category_list, {
                    id, categoryName, categoryDescription, categoryIcon, categoryColor
                }
            ]
        }
    };


    case EDIT_CATEGORY_ITEM:{
        const { categoryName, categoryDescription, categoryIcon, categoryColor } = action.payload
        const updatedCategory = state.category_list.map(category => {
        if (category.id != action.payload.id) {
            return category;
        } else {
            return {
            ...category,
                categoryName: categoryName, 
                categoryDescription: categoryDescription,
                categoryIcon: categoryIcon, 
                categoryColor: categoryColor,
            };
        }
        });
    
    return {
        ...state,
        category_list: updatedCategory
    };}

    case DELETE_CATEGORY_ITEM: {
        const { id } = action.payload
            return {
                ...state,
                category_list: state.category_list.filter((category) => category.id != id)
        };
    }


    case ADD_EXPENSE_DETAILS: {
        const {
            id, amount, dateValue, currentCategory, categoryDescription, categoryName
        } = action.payload
        return {
            ...state,
            expense_list: [
                ...state.expense_list, {
                    id, amount, dateValue, currentCategory, categoryDescription, categoryName
                }
            ]
        }
    };


    case EDIT_EXPENSE_DETAILS:{
        const { amount, dateValue, currentCategory, categoryDescription } = action.payload
        const updatedExpense = state.expense_list.map(expense => {
        if (expense.id != action.payload.id) {
            return expense;
        } else {
            return {
            ...expense,
                amount: amount, 
                dateValue: dateValue,
                currentCategory: currentCategory, 
                categoryDescription: categoryDescription,
            };
        }
        });
    
    return {
        ...state,
        expense_list: updatedExpense
    };}

    case DELETE_EXPENSE_DETAILS: {
        const { id } = action.payload
            return {
                ...state,
                expense_list: state.expense_list.filter((expense) => expense.id != id)
        };
    }




    
    case '@home/SCREEN_LOADER':
        return {
            ...state,
            screen_loader: action.payload.screen_loader,
        };
        default:
        return state;
    }
}
