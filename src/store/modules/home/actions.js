import { 
	ADD_CATEGORY_ITEM, EDIT_CATEGORY_ITEM, DELETE_CATEGORY_ITEM, 
	ADD_EXPENSE_DETAILS, EDIT_EXPENSE_DETAILS, DELETE_EXPENSE_DETAILS,
} from './actionTypes';

let nextCategoryId = 0;
let nextExpenseId = 0

export function addCategoryItem(params) {
    return {
        type: ADD_CATEGORY_ITEM,
        payload: {
            id: ++nextCategoryId,
            categoryName: params.categoryName,
            categoryDescription: params.categoryDescription,
            categoryIcon: params.categoryIcon,
            categoryColor: params.categoryColor,
        },
    };
};

export const editCategoryItem = params => {
	return {
		type: EDIT_CATEGORY_ITEM,
		payload: {
			id: params.id,
			categoryName: params.categoryName,
			categoryDescription: params.categoryDescription,
			categoryIcon: params.categoryIcon,
			categoryColor: params.categoryColor,
		},
	};
};

export const deleteCategoryItem = id => {
  return {
		type: DELETE_CATEGORY_ITEM,
		payload: {
			id
		},
	};
};



export function addExpenseDetails(params) {
    return {
        type: ADD_EXPENSE_DETAILS,
        payload: {
            id: ++nextExpenseId,
            amount: params.amount,
			categoryName: params.categoryName,
            dateValue: params.dateValue,
            currentCategory: params.currentCategory,
            categoryDescription: params.categoryDescription,
        },
    };
};

export const editExpenseDetails = params => {
	return {
		type: EDIT_EXPENSE_DETAILS,
		payload: {
			id: params.id,
			amount: params.amount,
            dateValue: params.dateValue,
            currentCategory: params.currentCategory,
            categoryDescription: params.categoryDescription,
		},
	};
};

export const deleteExpenseDetails = id => {
  return {
		type: DELETE_EXPENSE_DETAILS,
		payload: {
			id
		},
	};
};



export function screenLoader(screen_loader) {
	return {
		type: '@home/SCREEN_LOADER',
		payload: {
			screen_loader,
		},
	};
}
