const INITIAL_STATE = {
  current_user_profile: null,
  profile_list: [],
  app_theme_value: 'light',
  app_theme: {
    theme_type: 'light_theme',
    theme_value: 'light'
  }
};


export default function profile(state = INITIAL_STATE, action) {
    switch (action.type) {

        case '@profile/APP_THEME': {
            const {
                theme_type, theme_value,
            } = action.payload
                return {
                    ...state,
                    app_theme: {
                        theme_type, theme_value,
                    }
            };
        }
        
        case '@profile/APP_THEME_VALUE':
        return {
            ...state,
            app_theme_value: action.payload.app_theme_value,
        };
        
        case '@profile/CURRENT_USER_PROFILE':
        return {
            ...state,
            current_user_profile: action.payload.current_user_profile,
        };

        case '@profile/ADD_USER_PROFILE': {
            const { 
                id, userName, email, phoneNumber, password, confirmPassword,
            } = action.payload

            return {
                ...state,
                profile_list: [
                    ...state.profile_list, { 
                        id, userName, email, phoneNumber, password, confirmPassword,
                    }
                ]
            };
        }

        case '@profile/UPDATE_USER_PROFILE': {
            const { 
                id,
                user_name,
                user_icon,
            } = action.payload

            const updatedProfileList = state.profile_list.map(profile => {
                if (profile.id != action.payload.id) {
                  return profile;
                } else {
                  return {
                    ...profile, 
                    user_name,
                    user_icon,
                  };
                }
              });
           
            return {
                ...state,
                profile_list: updatedProfileList
            };}

        case '@profile/DELETE_USER_PROFILE': {
            const { id } = action.payload
                return {
                    ...state,
                    profile_list: state.profile_list.filter((profile) => profile.id != id)
            };
        }


        default:
        return state;
    }
}
