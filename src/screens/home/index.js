import React, {useState, useEffect, useRef} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, } from 'react-native'

import { Colors } from '../../global';
import { media } from '../../global/media';
import { Poppins } from '../../global/fontFamily';

import { useSelector } from 'react-redux';

import Icon from '../../utils/icons';
import ActionButton from '../../components/button/ActionButton';
import HomeHeader from '../../components/header/HomeHeader';


import moment from 'moment';
import LottieView from 'lottie-react-native';


const HomeScreen = ({navigation}) => {
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);


    const expense_list = useSelector(state => state.home.expense_list);
    

    const [totalExpense, setTotalExpense] = useState('');
    
    const [searchText, setSearchText] = useState('');
    const [isSearchFocused, setIsSearchFocused] = useState(false);

    const [expenseList, setExpenseList] = useState(expense_list);
    const [orgExpenseList, setOrgExpenseList] = useState(expense_list);

    useEffect(() => {
        setExpenseList(expense_list)
        setOrgExpenseList(expense_list)
        getTotalExpense()
    }, [expense_list])
    
    const getTotalExpense = () => {
        const arr = expense_list

        const total = arr.reduce((prev,next) => Number(prev) + Number(next.amount),0);
        setTotalExpense(total)
    }

    const searchFilterFunction = (text) => {

        var expense = expense_list
        if (text) {
          const newData = expense.filter(
            function (item) {
                const itemData = item?.amount ? item?.amount.toUpperCase() : ''.toUpperCase();
                const descData = item?.categoryDescription ? item?.categoryDescription?.toUpperCase() : ''.toUpperCase() 
                const categoryData = item?.currentCategory?.categoryName ? item?.currentCategory?.categoryName?.toUpperCase() : ''.toUpperCase() 
                const textData = text.toUpperCase();
                return itemData.indexOf(textData) > -1 || descData.indexOf(textData) > -1 || categoryData.indexOf(textData) > -1;
          });


          
          setExpenseList(newData);
          setSearchText(text);
        } else {

          setExpenseList(orgExpenseList);
          setSearchText(text);
        }
    };



    return (
        <SafeAreaView style={styles.container} >
            
            <HomeHeader />

            <ScrollView>
                <View style={{flex: 1, padding: 20, marginBottom: 80, paddingTop: 10, }} >

               <View style={{backgroundColor: Colors[app_theme?.theme_value].primaryColor, flexDirection:'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 10, padding: 15, paddingHorizontal: 20, paddingVertical: 20, marginBottom: 10}} >
                    <View>
                        <Text style={[styles.categoryItemDescription, {color: Colors[app_theme?.theme_value].commonWhite}]}>Total Expenses</Text>
                        <Text style={[styles.categoryItemTitle, {fontSize: 28, marginTop: 10, fontFamily: Poppins.Bold, color: Colors[app_theme?.theme_value].commonWhite}]}>{"\u20B9"} {totalExpense}</Text>
                    </View>
                    <Image source={media.logo} style={{height: 100, width: 100, borderRadius: 10}} />
               </View>

                <View style={{flexDirection: 'row', alignItems: 'center'}} >
                    <View style={styles.inputContainer} >
                        <Icon type={"AntDesign"} name={"search1"}  size={32} color={Colors[app_theme?.theme_value].grayColor} />
                        <TextInput
                            placeholder='Search'
                            placeholderTextColor={Colors[app_theme?.theme_value]?.black}
                            style={styles.inputStyle}
                            value={searchText}
                            onChangeText={(text) => {searchFilterFunction(text)}}
                            onFocus={() => {console.log("focus"); setIsSearchFocused(true); }}
                            onBlur={() => {setSearchText(''); console.log("bluer"); setIsSearchFocused(false); setExpenseList(orgExpenseList) }}
                        />
                    </View>

                    {isSearchFocused ?
                    <TouchableOpacity onPress={() => {setSearchText(''); setIsSearchFocused(false);  setExpenseList(orgExpenseList);}} >
                         <Text style={styles.cancelText} >Cancel</Text>
                    </TouchableOpacity>
                        
                    : null}
                </View>


                {expenseList?.map((item, index) => (
                    <TouchableOpacity 
                        key={index}
                        activeOpacity={0.8}
                        onPress={() => {navigation.navigate('EditExpense', {details: item})}} 
                        style={styles.categoryItemContainer} >
                        <View style={[styles.categoryItemIconContainer, {backgroundColor: item?.currentCategory?.categoryColor?.bgColor || 'white'}]} >
                            <Icon type={item?.currentCategory?.categoryIcon?.type} name={item?.currentCategory?.categoryIcon?.title}  size={32} color={item?.currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', }} >
                            <View style={{flex: 1, }} >
                                <Text numberOfLines={1} style={styles.categoryItemTitle} >{item?.currentCategory?.categoryName}</Text>
                                <Text numberOfLines={1} style={styles.categoryItemDescription} >{item?.categoryDescription}</Text>
                                <Text numberOfLines={1} style={styles.categoryItemDescription} >{item?.dateValue && moment(item?.dateValue).format('Do MMM YYYY, h:mm a')}</Text>
                            </View>
                            <View>
                                <Text style={styles.categoryItemTitle} >{"\u20B9"} {item.amount}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                ))}

                {expenseList?.length == 0 && (
                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 20}} >
                    
                            <LottieView
                                source={media.empty_lottie} 
                                autoPlay 
                                loop 
                                style={{height: 200, width: 200, marginBottom: 12}}
                            />
                            <Text style={styles.modalTitle}>Oopss!!!</Text>
                            <Text style={styles.modalSubTitle}>No Expenses added!</Text>
                    </View>
                )}
                </View>
            </ScrollView>


     

            <ActionButton
                type="add"
                onPress={() => {navigation.navigate('AddExpense')}} 
            />
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    categoryItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        borderColor: 'black',
        backgroundColor: Colors[theme]?.gray,
        padding: 15,
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 7,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    categoryItemIconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 4,
    },
    categoryItemTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    categoryItemDescription: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: Colors[theme]?.white,
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: 14,
        paddingVertical: 2,
        marginVertical: 10,
        marginBottom: 20,
        borderRadius: 10,
        flex: 1,
        backgroundColor: Colors[theme].gray
    },
    inputStyle: {
        flex: 1,
        paddingLeft: 12,
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    filterContainer: {
        height: 45,
        width: 45,
        marginLeft: 4,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cancelText: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
        marginBottom: 10,
        paddingLeft: 10
    },
    modalSubTitle: {
        fontSize: 16,
        fontFamily: Poppins.Light,
        color: Colors[theme]?.white,
    },
    modalTitle: {
        fontSize: 24,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    }
})

export default HomeScreen