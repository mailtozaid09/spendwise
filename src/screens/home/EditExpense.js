import React, { useState, useEffect, useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';
import { useDispatch, useSelector } from 'react-redux';
import { deleteExpenseDetails, editExpenseDetails, } from '../../store/modules/home/actions';

import Icon from '../../utils/icons';


import CategorySheet from '../../components/bottomSheet/CategorySheet';
import PrimaryButton from '../../components/button/PrimaryButton';
import AlertModal from '../../components/modal/AlertModal';
import moment from 'moment';
import DatePicker from 'react-native-date-picker';


const EditExpenseScreen = (props) => {

    const defaultCategory = {
        "categoryColor": {
            "bgColor": "#ADDCCC", 
            "iconColor": "#015555",
        }, 
        "categoryDescription": "dsdsa", 
        "categoryIcon": {
            "title": "dots-three-horizontal",
             "type": "Entypo"
        }, 
        "categoryName": "Others"
    }

    const dispatch = useDispatch();

    const refRBSheet = useRef();

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    const [errors, setErrors] = useState({});
    const [showAlerModal, setShowAlerModal] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');

    const [showDatePicker, setShowDatePicker] = useState(false);
    const [date, setDate] = useState(new Date());
    const [expenseId, setExpenseId] = useState('');
    const [amount, setAmount] = useState('');
    const [dateValue, setDateValue] = useState(new Date());
    const [categoryDescription, setCategoryDescription] = useState('');
    const [currentCategory, setCurrentCategory] = useState(defaultCategory);

    useEffect(() => {
        console.log("dadsa=========");
        var details = props?.route?.params?.details
        setExpenseId(details?.id)
        setAmount(details?.amount)
        //setDate(details?.dateValue)
        setDateValue(details?.dateValue)
        setCurrentCategory(details?.currentCategory)
        setCategoryDescription(details?.categoryDescription)
    }, [])
    
    const selectCurrentCategory = (category) => {
        setCurrentCategory(category)
        refRBSheet.current.close()
    }

    const deleteEnpenseFunction = () => {
        console.log("delete");
        setAlertType('Delete')
        setAlertTile("Are you sure!");
        setAlertDescription("You want to delete?");
        setShowAlerModal(true)
    }


    const saveExpenseFunction = () => {
        console.log("save");
        // console.log('====================================');
        // console.log("dateValue : ", dateValue);
        // console.log("amount : ", amount);
        // console.log("currentCategory : ", currentCategory);
        // console.log("categoryDescription : ", categoryDescription);
        // console.log('====================================');

        var isValid = true
        if(!amount){
            console.log("Please enter a valid amount");
            isValid = false
            setErrors((prev) => {
                return {...prev, amount: 'Please enter a valid amount'}
            })
        }

        if(!categoryDescription){
            console.log("Please enter a valid description");
            isValid = false
            setErrors((prev) => {
                return {...prev, description: 'Please enter a valid description'}
            })
        }

        if(isValid){
            console.log("can edit");
           
    
            setAlertType('Edit')
            setAlertTile("Are you sure!");
            setAlertDescription("You want to edit?");
    
            setShowAlerModal(true)
        }
        
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
              
                <View style={{flex: 1, padding: 20, paddingTop: 10}} >
                    

                    <Text style={styles.categoryHeading} >Date</Text>
                    <View style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={"Ionicons"} name={"calendar"}  size={32} color={currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <TouchableOpacity 
                            onPress={() => {setShowDatePicker(true)}}
                            style={{ flex: 1, padding: 10, borderRadius: 10, justifyContent: 'center'}} >
                            <Text style={{fontSize: 16, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value]?.white,}} >{moment(date).format('Do MMM YYYY, h:mm a')}</Text>
                        </TouchableOpacity>
                      
                        <DatePicker
                            modal
                            date={date}
                            open={showDatePicker}
                            maximumDate={new Date()}
                            onConfirm={(date) => {
                                setShowDatePicker(false)
                                setDate(date)
                            }}
                            onCancel={() => {
                                setShowDatePicker(false)
                            }}
                        />
                    </View>
                  
                   
                    <Text style={styles.categoryHeading} >Amount</Text>
                    <View style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={"FontAwesome"} name={"rupee"}  size={32} color={currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <TextInput 
                            placeholder='Enter amount'
                            keyboardType='number-pad'
                            value={amount}
                            onChangeText={(text) => {setAmount(text); setErrors({})}}
                            placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                            style={[styles.inputStyle]}
                        />
                    </View>
                    {errors.amount && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.amount}</Text>}

                    <Text style={styles.categoryHeading} >Category</Text>
                    <TouchableOpacity 
                        onPress={() => refRBSheet.current.open()} 
                        style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={currentCategory?.categoryIcon?.type} name={currentCategory?.categoryIcon?.title}  size={32} color={currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <Text style={styles.heading} >{currentCategory?.categoryName}</Text>
                    </TouchableOpacity>
                    
                    <Text style={styles.categoryHeading} >Description</Text>
                    <View style={styles.descriptionContainer} >
                        <View style={styles.iconContainer} >
                            <Icon type={"MaterialCommunityIcons"} name={"text-long"}  size={38} color={currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <TextInput 
                            placeholder='Write a Description'
                            value={categoryDescription}
                            onChangeText={(text) => {setCategoryDescription(text); setErrors({})}}
                            placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                            style={[styles.inputStyle]}
                        />
                    </View>
                    {errors.description && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.description}</Text>}

                    <PrimaryButton
                        title="Delete Expense" 
                        onPress={() => {
                            deleteEnpenseFunction()
                        }}
                        isDelete={true}
                        color={Colors[app_theme?.theme_value]?.redColor}
                        buttonStyle={{marginVertical: 15, }}
                    />
                    <PrimaryButton
                        title="Edit Expense" 
                        onPress={() => {
                            saveExpenseFunction()
                        }}
                        
                        color={Colors[app_theme?.theme_value]?.primaryColor}
                        buttonStyle={{ marginBottom: 80}}
                    />
                </View>
              
            </ScrollView>

            {showAlerModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onDelete={() => {dispatch(deleteExpenseDetails(expenseId));  props.navigation.goBack()}}
                    onCancel={() => {setShowAlerModal(false);}}
                    onEdit={() => {
                        dispatch(editExpenseDetails({
                            id: expenseId,
                            amount: amount,
                            dateValue: date,
                            currentCategory: currentCategory,
                            categoryDescription: categoryDescription,
                        }));
                        setShowAlerModal(false); 
                        props.navigation.goBack();
                    }}
                    closeModal={() => {setShowAlerModal(false); }} 
                />
            }
            
            <CategorySheet 
                refRBSheet={refRBSheet} 
                currentCategory={currentCategory}
                selectCurrentCategory={(category) => {
                    selectCurrentCategory(category)
                }} 
            /> 
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    categoryContainer: {
        marginBottom: 15,
    },
    categoryHeading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
        marginTop: 10,
        marginBottom: 2,
    },
    descriptionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        marginBottom: 6,
    },
    iconContainer: {
        height: 40, 
        width: 40, 
        justifyContent: 'center', 
        alignItems: 'center',
        marginRight: 10
    },
    inputStyle: {
        padding: 15,
        paddingVertical: 10,
        paddingTop: 0,
        paddingLeft: 0,
        paddingBottom: 0,
        marginTop: 4,
        fontSize: 16,
        flex: 1,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    heading: {
        padding: 15,
        paddingVertical: 10,
        paddingTop: 0,
        paddingLeft: 0,
        marginTop: 4,
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    }
})

export default EditExpenseScreen