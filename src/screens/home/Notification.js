import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../global';
import { media } from '../../global/media';
import { screenWidth } from '../../global/constants';
import { Poppins } from '../../global/fontFamily';

import LottieView from 'lottie-react-native';
import { useSelector } from 'react-redux';

const NotificationScreen = ({navigation}) => {
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);
    
    return (
        <SafeAreaView style={styles.container} >
            <View style={{paddingVertical: 30,  width: '100%' }}>
            <ScrollView>
                <View style={{ alignItems: 'center', justifyContent: 'center', paddingHorizontal: 30}} >
                        <LottieView
                            source={media.notification_lottie} 
                            autoPlay 
                            loop 
                            style={{height: screenWidth/1.5, width: screenWidth/1.5, marginBottom: 20}}
                        />
                    
                    <Text style={styles.title} >Nothing here!!!</Text>
                    <Text style={styles.subtitle} >We'll let notify you when there is something new</Text>
                </View>
            </ScrollView>
            </View>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[theme]?.themeColor,
    },
    title: {
        fontSize: 22,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    },
    subtitle: {
        fontSize: 16,
        lineHeight: 20,
        textAlign: 'center',
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
})


export default NotificationScreen