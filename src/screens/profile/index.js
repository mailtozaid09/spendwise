import React, {useState, useEffect, useCallback} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../global';
import { media } from '../../global/media';
import { Poppins } from '../../global/fontFamily';
import AlertModal from '../../components/modal/AlertModal';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { userLoggedIn } from '../../store/modules/auth/actions';
import { useDispatch, useSelector } from 'react-redux';
import ThemeModal from '../../components/modal/ThemeModal';
import { get, save } from '../../utils/storage';
import { setAppTheme, setAppThemeValue } from '../../store/modules/profile/actions';

const ProfileScreen = ({navigation}) => {

    const dispatch = useDispatch();
    const themes = useColorScheme();
    


    const [currentTheme, setCurrentTheme] = useState('');

    const [showThemeModal, setShowThemeModal] = useState(false);
    const [showAlertModal, setShowAlertModal] = useState(false);

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');


    const [themeValue, setThemeValue] = useState('light');
    const [initialValue, setInitialValue] = useState(0);

    const styles = styling(themeValue);

    const themeOperations = theme => {
        console.log("theme : ",theme);

        if(theme == null){
            dispatch(setAppTheme({
                theme_type: 'light_theme',
                theme_value: 'light',
            }))
        }
        switch (theme) {
            case 'dark':
                setTheme(theme, false);
                setInitialValue(2);
                dispatch(setAppTheme({
                    theme_type: 'dark_theme',
                    theme_value: theme,
                }))
            return;
            case 'light':
                setTheme(theme, false);
                setInitialValue(1);
                dispatch(setAppTheme({
                    theme_type: 'light_theme',
                    theme_value: theme,
                }))
            return;
            case 'default':
                setTheme(themes, true);
                setInitialValue(3);
                dispatch(setAppTheme({
                    theme_type: 'default_theme',
                    theme_value: themes,
                }))
            return;
        }
    };
  
    const getAppTheme = useCallback(async () => {
      const theme = await get('Theme');
      const isDefault = await get('IsDefault');
      isDefault ? themeOperations('default') : themeOperations(theme);
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);
  
    const setTheme = useCallback(async (theme, isDefault) => {
        save('Theme', theme);
        save('IsDefault', isDefault);
        setThemeValue(theme);
    }, []);
  
    useEffect(() => {
        getAppTheme();
    }, [getAppTheme]);
    
    const profile_options = [
        {
            title: 'Theme',
            icon: media.dark_mode,
        },{
            title: 'Expense',
            icon: media.account,
        },
        {
            title: 'Summary',
            icon: media.summary_active,
        },
        {
            title: 'Categories',
            icon: media.category_active,
        },
        {
            title: 'Logout',
            icon: media.logout,
        },
    ]

    const onProfileOptions = (option) => {

        if(option == 'Theme'){
            setShowThemeModal(true)
        }else if(option == 'Expense'){
            navigation.navigate('HomeStack')
        }else if(option == 'Summary'){
            navigation.navigate('SummaryStack')
        }else if(option == 'Categories'){
            navigation.navigate('CategoryStack')
        }else if(option == 'Logout'){
            setAlertType('Logout')
            setAlertTile("Are you sure!");
            setAlertDescription("You want to logout?");
            setShowAlertModal(true)
        }
        
    }


    const logoutFunction = () => {
        dispatch(userLoggedIn(false))
        AsyncStorage.clear()
        navigation.navigate('LoginStack')
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                    
                <View style={styles.mainContainer} >
                    <View style={styles.userDetails} >
                        <View style={styles.userImgContainer} >
                            <Image source={media.user_img} style={styles.userImg} />
                        </View>
                        <View style={styles.userDetailsContent} >
                            <Text style={styles.title} >Zaid Ahmed</Text>
                            <Text style={styles.subtitle} >9027346976</Text>
                            <Text style={styles.subtitle} >mailtozaid09@gmail.com</Text>
                        </View>
                    </View>


                    <View style={styles.profileOptions} >
                        {profile_options?.map((item, index) => (
                            <TouchableOpacity  
                                key={index}
                                onPress={() => {onProfileOptions(item.title)}}
                                style={[styles.profileOptionsContainer, item.title == 'Logout' && {borderBottomWidth: 0}]} 
                            >
                                <View style={[styles.optionIconContainer, item.title == 'Logout' && {backgroundColor: Colors[themes]?.lightRedColor,}]} >
                                    <Image source={item.icon} style={{height: 28, width: 28, }} />
                                </View>
                                <Text style={styles.optionTitle} >{item.title}</Text>
                            </TouchableOpacity>
                        ))}
                    </View>
                </View>

                {showThemeModal &&
                    <ThemeModal 
                        currentTheme={currentTheme}
                        closeModal={() => {
                            setShowThemeModal(false); 
                        }} 
                        themeOperations={(theme) => themeOperations(theme)}
                        onChangeTheme={(theme) => (setCurrentTheme(theme))}
                    /> 
                }

                {showAlertModal && 
                    <AlertModal
                        alertType={alertType}
                        title={alertTile}
                        description={alertDescription}
                        onLogout={() => {setShowAlertModal(false); logoutFunction()}}
                        closeModal={() => {setShowAlertModal(false); }} 
                    />
                }
                </ScrollView>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    mainContainer: {
        padding: 20,
    },
    userDetails: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    userDetailsContent: {
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 10,
    },
    userImgContainer: {
        height: 120,
        width: 120,
        borderRadius: 60,
        borderWidth: 2,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: Colors[theme]?.primaryColor,
    },
    userImg: {
        height: 110,
        width: 110,
        borderRadius: 55,
    },
    title: {
        color: Colors[theme]?.white,
        fontSize: 22,
        fontFamily: Poppins.SemiBold
    },
    subtitle: {
        color: Colors[theme]?.white,
        fontSize: 16,
        fontFamily: Poppins.Medium
    },
    optionTitle: {
        color: Colors[theme]?.white,
        fontSize: 18,
        fontFamily: Poppins.Medium
    },
    profileOptions: {
        padding: 10,
        paddingHorizontal: 20,
        borderRadius: 10,
        marginTop: 10,
        backgroundColor: Colors[theme]?.gray,
    },
    profileOptionsContainer: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingVertical: 12,
        borderBottomWidth: 1,
        borderColor: Colors[theme]?.primaryColor,
    },
    optionIconContainer: {
        height: 50,
        width: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        backgroundColor: Colors[theme]?.primaryLightColor,
    }
})

export default ProfileScreen