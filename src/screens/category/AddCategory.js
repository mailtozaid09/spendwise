import React, { useState, useEffect, useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';
import { useDispatch, useSelector } from 'react-redux';
import { addCategoryItem, } from '../../store/modules/home/actions';
import { Entertainment_icon_array, Miscellaneous_icon_array, colors_array, food_icon_array, shopping_icon_array, travel_icon_array } from '../../global/sampleData';


import Icon from '../../utils/icons';
import AlertModal from '../../components/modal/AlertModal';
import PrimaryButton from '../../components/button/PrimaryButton';


const AddCategoryScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);


    const [errors, setErrors] = useState({});
    const [showAlerModal, setShowAlerModal] = useState(false);

    const [categoryIcon, setCategoryIcon] = useState(null);
    const [categoryColor, setCategoryColor] = useState({});

    const [categoryName, setCategoryName] = useState('');
    const [categoryDescription, setCategoryDescription] = useState('');

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');

    useEffect(() => {
        var random_color = colors_array[Math.floor(Math.random()*colors_array.length)];
        var random_icon = food_icon_array[Math.floor(Math.random()*food_icon_array.length)];
       
        setCategoryIcon(random_icon)
        setCategoryColor(random_color)
    }, [])



    const saveCategoryFunction = () => {
        console.log("addd");
        // console.log('====================================');
        // console.log("categoryName : ", categoryName);
        // console.log("categoryDescription : ", categoryDescription);
        // console.log("categoryIcon : ", categoryIcon);
        // console.log("categoryColor : ", categoryColor);
        // console.log('====================================');


        var isValid = true
        if(!categoryName){
            console.log("Please enter a valid categoryName");
            isValid = false
            setErrors((prev) => {
                return {...prev, categoryName: 'Please enter a valid name'}
            })
        }

        if(!categoryDescription){
            console.log("Please enter a valid description");
            isValid = false
            setErrors((prev) => {
                return {...prev, description: 'Please enter a valid description'}
            })
        }

        if(isValid){
            console.log("can addd");
            dispatch(addCategoryItem({
                categoryName: categoryName,
                categoryDescription: categoryDescription,
                categoryIcon: categoryIcon,
                categoryColor: categoryColor,
            }))

            setAlertType('Add')
            setAlertTile("Success");
            setAlertDescription("A new category has been added!");
            setShowAlerModal(true)
    
        }
        
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
              
                <View style={{flex: 1, padding: 20}} >
                    <View style={styles.categoryHeader} >
                        <View style={[styles.categoryItemIconContainer, {backgroundColor: categoryColor?.bgColor || 'white'}]} >
                            <Icon type={categoryIcon?.type} name={categoryIcon?.title}  size={50} color={categoryColor?.iconColor} />
                        </View>
                        <View style={{flex: 1, height: 110,}} >
                            <Text style={styles.categorySubHeading} >Category Name</Text>
                            <TextInput 
                                placeholder='Category Name'
                                onChangeText={(text) => {setCategoryName(text); setErrors({})}}
                                placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                                style={styles.inputStyle}
                            />
                            {errors.categoryName && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.categoryName}</Text>}
                        </View>
                    </View>

                    <View style={styles.descriptionContainer} >
                        <Icon type={'MaterialCommunityIcons'} name={'text-long'}  size={30} color={Colors[app_theme?.theme_value].white} style={{marginRight: 10}} />
                        <TextInput 
                            placeholder='Description'
                            onChangeText={(text) => {setCategoryDescription(text); setErrors({})}}
                            placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                            style={[styles.inputStyle, {fontSize: 14, flex: 1}]}
                        />
                    </View>

                    {errors.description && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.description}</Text>}


                    <View style={styles.itemContainer} >
                        <Text style={[styles.categoryHeading, {fontSize: 16}]} >Category Color</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {colors_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryColor(item)}}
                                    style={{height: 50, width: 50, borderWidth: 1, borderColor: categoryColor.iconColor == item.iconColor ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.gray, borderRadius: 25, marginTop: 10, marginRight: 15, alignItems: 'center', justifyContent: 'center'}} >
                                    <View style={{height: 40, width: 40, borderRadius: 25, backgroundColor: item.iconColor}}  />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>

                    <View style={styles.itemContainer} >
                        <Text style={[styles.categoryHeading, {fontSize: 16}]} >Category Icon</Text>

                        <Text style={styles.categoryHeading} >Food</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {food_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Travel</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {travel_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>

                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Shopping</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {shopping_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Entertainment</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap',  alignItems: 'center'}} >
                            {Entertainment_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Miscellaneous</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {Miscellaneous_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>
                    <PrimaryButton
                        title="Add Category" 
                        onPress={() => {
                            saveCategoryFunction()
                        }}
                        
                        color={Colors[app_theme?.theme_value]?.primaryColor}
                        buttonStyle={{marginTop: 15}}
                    />
                </View>
              
                
            </ScrollView>

            {showAlerModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onAdd={() => {setShowAlerModal(false); navigation.goBack();}}
                    closeModal={() => {setShowAlerModal(false); }} 
                />
            }

            
                    
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    categoryContainer: {
        marginBottom: 15,
    },
    categoryHeading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
        marginBottom: 10,
    },
    categorySubHeading: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    },
    categoryItemIconContainer: {
        height: 80,
        width: 80,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
    },
    categoryHeader: {
        flexDirection: 'row',
        alignItems: 'center',
    }, 
    descriptionContainer: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    itemContainer: {
        marginTop: 10,
        width: '100%',
        flex: 1,
    },
    iconContainer: {
        height: 50, 
        width: 50, 
        borderRadius: 25, 
        marginRight: 20, 
        marginTop: 10, 
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputStyle: {
        padding: 15,
        paddingVertical: 10,
        paddingLeft: 0,
        marginTop: 4,
        fontSize: 18,
        marginRight: 20,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    }
})

export default AddCategoryScreen