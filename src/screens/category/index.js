import React, { useState, useEffect, useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../global';
import { media } from '../../global/media';
import { Poppins } from '../../global/fontFamily';
import { useDispatch, useSelector } from 'react-redux';


import Icon from '../../utils/icons';
import ActionButton from '../../components/button/ActionButton';

import LottieView from 'lottie-react-native';


const CategoryScreen = ({navigation}) => {

    const dispatch = useDispatch();

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    const category_list = useSelector(state => state.home.category_list);

    useEffect(() => {

    }, [])

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={{flex: 1, padding: 20}} >
                    <View>

                        {category_list?.map((item, index) => (
                            <TouchableOpacity 
                                key={index}
                                activeOpacity={0.8}
                                onPress={() => {navigation.navigate('EditCategory', {details: item})}} 
                                style={styles.categoryItemContainer} >
                                <View style={[styles.categoryItemIconContainer, {backgroundColor: item.categoryColor?.bgColor || 'white'}]} >
                                    <Icon type={item.categoryIcon?.type} name={item.categoryIcon?.title}  size={32} color={item.categoryColor?.iconColor} />
                                </View>
                                <View style={{flex: 1}} >
                                    <Text style={styles.categoryItemTitle} >{item.categoryName}</Text>
                                    <Text numberOfLines={1} style={styles.categoryItemDescription} >{item.categoryDescription}</Text>
                                </View>
                            </TouchableOpacity>
                        ))}

                        {category_list?.length == 0 && (
                            <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 20}} >
                                <LottieView
                                    source={media.empty_lottie} 
                                    autoPlay 
                                    loop 
                                    style={{height: 200, width: 200, marginBottom: 12}}
                                />
                                <Text style={styles.modalTitle}>Oopss!!!</Text>
                                <Text style={styles.modalSubTitle}>No Categores added!</Text>
                            </View>
                        )}
                    </View>
                </View> 
            </ScrollView>
            <ActionButton 
                type="add"
                onPress={() => {navigation.navigate('AddCategory')}} 
            />
            
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    categoryItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        borderColor: 'black',
        backgroundColor: Colors[theme]?.gray,
        padding: 15,
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 7,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    categoryItemIconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 4,
    },
    categoryItemTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    categoryItemDescription: {
        fontSize: 14,
        fontFamily: Poppins.Regular,
        color: Colors[theme]?.white,
    },
    modalSubTitle: {
        fontSize: 16,
        fontFamily: Poppins.Light,
        color: Colors[theme]?.white,
    },
    modalTitle: {
        fontSize: 24,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    }
})

export default CategoryScreen