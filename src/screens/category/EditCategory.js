import React, { useState, useEffect, useRef } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, TextInput, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';
import { useDispatch, useSelector } from 'react-redux';
import { deleteCategoryItem, editCategoryItem, } from '../../store/modules/home/actions';
import { Entertainment_icon_array, Miscellaneous_icon_array, colors_array, food_icon_array, shopping_icon_array, travel_icon_array } from '../../global/sampleData';

import Icon from '../../utils/icons';
import PrimaryButton from '../../components/button/PrimaryButton';
import AlertModal from '../../components/modal/AlertModal';


const EditCategoryScreen = (props) => {

    const dispatch = useDispatch();

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);


    const [errors, setErrors] = useState({});
    const [showAlerModal, setShowAlerModal] = useState(false);

    const [categoryIcon, setCategoryIcon] = useState(null);
    const [categoryColor, setCategoryColor] = useState({});

    const [categoryId, setCategoryId] = useState('');
    const [categoryName, setCategoryName] = useState('');
    const [categoryDescription, setCategoryDescription] = useState('');

    const [alertType, setAlertType] = useState('');
    const [alertTile, setAlertTile] = useState('');
    const [alertDescription, setAlertDescription] = useState('');

    useEffect(() => {
        var details = props?.route?.params?.details
        setCategoryId(details?.id)
        setCategoryIcon(details?.categoryIcon)
        setCategoryColor(details?.categoryColor)
        setCategoryName(details?.categoryName)
        setCategoryDescription(details?.categoryDescription)
    }, [])

    


    const deleteCategoryFunction = () => {
        console.log("delete");
        setAlertType('Delete')
        setAlertTile("Are you sure!");
        setAlertDescription("You want to delete?");
        setShowAlerModal(true)
    }


    const editCategoryFunction = () => {
        console.log("edit");
        // console.log('====================================');
        // console.log("categoryName : ", categoryName);
        // console.log("categoryDescription : ", categoryDescription);
        // console.log("categoryIcon : ", categoryIcon);
        // console.log("categoryColor : ", categoryColor);
        // console.log('====================================');

        var isValid = true
        if(!categoryName){
            console.log("Please enter a valid categoryName");
            isValid = false
            setErrors((prev) => {
                return {...prev, categoryName: 'Please enter a valid name'}
            })
        }

        if(!categoryDescription){
            console.log("Please enter a valid description");
            isValid = false
            setErrors((prev) => {
                return {...prev, description: 'Please enter a valid description'}
            })
        }

        if(isValid){
            console.log("can edit");

            setAlertType('Edit')
            setAlertTile("Are you sure!");
            setAlertDescription("You want to edit?");
    
            setShowAlerModal(true)
            
        }

        
    }

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
              
                <View style={{flex: 1, padding: 20}} >
                    <View style={styles.categoryHeader} >
                        <View style={[styles.categoryItemIconContainer, {backgroundColor: categoryColor?.bgColor || 'white'}]} >
                            <Icon type={categoryIcon?.type} name={categoryIcon?.title}  size={50} color={categoryColor?.iconColor} />
                        </View>
                        <View style={{flex: 1, height: 110}} >
                            <Text style={styles.categorySubHeading} >Category Name</Text>
                            <TextInput 
                                placeholder='Category Name'
                                value={categoryName}
                                onChangeText={(text) => {setCategoryName(text); setErrors({})}}
                                placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                                style={styles.inputStyle}
                            />
                            {errors.categoryName && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.categoryName}</Text>}
                            
                        </View>
                    </View>

                    <View style={styles.descriptionContainer} >
                        <Icon type={'MaterialCommunityIcons'} name={'text-long'}  size={30} color={Colors[app_theme?.theme_value].white} style={{marginRight: 10}} />
                        <TextInput 
                            placeholder='Description'
                            value={categoryDescription}
                            onChangeText={(text) => {setCategoryDescription(text); setErrors({})}}
                            placeholderTextColor={Colors[app_theme?.theme_value]?.gray}
                            style={[styles.inputStyle, {fontSize: 14, flex: 1}]}
                        />
                    </View>
                    {errors.description && <Text style={{fontSize: 14, fontFamily: Poppins.Medium, color: Colors[app_theme?.theme_value].redColor }} >{errors?.description}</Text>}


                    <View style={styles.itemContainer} >
                        <Text style={[styles.categoryHeading, {fontSize: 16}]} >Category Color</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {colors_array?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index} 
                                    onPress={() => {setCategoryColor(item)}}
                                    style={{height: 45, width: 45, borderWidth: 1, borderColor: categoryColor.iconColor == item.iconColor ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.gray, borderRadius: 25, marginTop: 10, marginRight: 15, alignItems: 'center', justifyContent: 'center'}} >
                                    <View style={{height: 35, width: 35, borderRadius: 25, backgroundColor: item.iconColor}}  />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>

                    <View style={styles.itemContainer} >
                        <Text style={[styles.categoryHeading, {fontSize: 16}]} >Category Icon</Text>

                        <Text style={styles.categoryHeading} >Food</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {food_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index} 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Travel</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {travel_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index} 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>

                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Shopping</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {shopping_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index} 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Entertainment</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap',  alignItems: 'center'}} >
                            {Entertainment_icon_array?.map((item, index) => (
                                <TouchableOpacity 
                                    key={index} 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <View style={styles.itemContainer} >
                        
                        <Text style={styles.categoryHeading} >Miscellaneous</Text>

                        <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                            {Miscellaneous_icon_array?.map((item, index) => (
                                <TouchableOpacity
                                    key={index} 
                                    onPress={() => {setCategoryIcon(item)}}
                                    style={[styles.iconContainer, {backgroundColor: categoryIcon?.title == item.title ? Colors[app_theme?.theme_value]?.primaryColor : Colors[app_theme?.theme_value]?.primaryLightColor,}]} >
                                    <Icon type={item.type} name={item.title}  size={30} color={Colors[app_theme?.theme_value].commonWhite} />
                                </TouchableOpacity>
                            ))}
                        </View>
                    </View>


                    <PrimaryButton
                        title="Delete Category" 
                        onPress={() => {
                            deleteCategoryFunction()
                        }}
                        isDelete={true}
                        color={Colors[app_theme?.theme_value]?.redColor}
                        buttonStyle={{marginVertical: 15,}}
                    />
                    <PrimaryButton
                        title="Edit Category" 
                        onPress={() => {
                            editCategoryFunction()
                        }}
                        
                        color={Colors[app_theme?.theme_value]?.primaryColor}
                        buttonStyle={{}}
                    />
                </View>

            </ScrollView>

            {showAlerModal && 
                <AlertModal
                    alertType={alertType}
                    title={alertTile}
                    description={alertDescription}
                    onDelete={() => {dispatch(deleteCategoryItem(categoryId));  props.navigation.goBack()}}
                    onCancel={() => {setShowAlerModal(false);}}
                    onEdit={() => {
                        setShowAlerModal(false); 
                        props.navigation.goBack();
                        dispatch(editCategoryItem({
                            id: categoryId,
                            categoryName: categoryName,
                            categoryDescription: categoryDescription,
                            categoryIcon: categoryIcon,
                            categoryColor: categoryColor,
                        }))
                    }}
                    closeModal={() => {setShowAlerModal(false); }} 
                />
            }

        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    categoryContainer: {
        marginBottom: 15,
    },
    categoryHeading: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
        marginBottom: 10,
    },
    categorySubHeading: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    },
    categoryItemIconContainer: {
        height: 80,
        width: 80,
        borderRadius: 40,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
    },
    categoryHeader: {
        flexDirection: 'row',
        alignItems: 'center',
    }, 
    descriptionContainer: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
    },
    itemContainer: {
        marginTop: 10,
        width: '100%',
        flex: 1,
    },
    iconContainer: {
        height: 50, 
        width: 50, 
        borderRadius: 25, 
        marginRight: 20, 
        marginTop: 10, 
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputStyle: {
        padding: 15,
        paddingVertical: 10,
        paddingLeft: 0,
        marginTop: 4,
        fontSize: 18,
        marginRight: 20,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    }
})

export default EditCategoryScreen
