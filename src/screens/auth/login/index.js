import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../../global';

const LoginScreen = ({navigation}) => {
    const themes = useColorScheme();
    const styles = styling(themes);
    
    return (
        <SafeAreaView style={styles.container} >
            <View>
                <Text>LoginScreen</Text>
            </View>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
})

export default LoginScreen