import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../../global';
import PrimaryButton from '../../../components/button/PrimaryButton';
import { Poppins } from '../../../global/fontFamily';
import { media } from '../../../global/media';
import { screenWidth } from '../../../global/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { userLoggedIn } from '../../../store/modules/auth/actions';

const WelcomeScreen = ({navigation}) => {
    const dispatch = useDispatch();

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);
    

    const saveUserDetails = () => {
        dispatch(userLoggedIn(true))
        AsyncStorage.setItem("user_details", JSON.stringify("logged_in"));
        navigation.navigate('Tabbar')
    }

  


    return (
        <SafeAreaView style={styles.container} >
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center' }} >
                <Image source={media.welcome} style={{height: screenWidth, width: screenWidth, resizeMode: 'contain', marginTop: 20}} />
            </View>
            <View style={{alignItems: 'center', padding: 12}} >
                <Text style={styles.heading} >Spend Smarter{'\n'}Save More</Text>
                <PrimaryButton
                    title="Get Started" 
                    onPress={() => {
                        saveUserDetails();
                    }}
                    
                    color={Colors[app_theme?.theme_value]?.primaryColor}
                    buttonStyle={{}}
                />
            </View>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    heading: {
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 20,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.primaryColor,
    }
})

export default WelcomeScreen