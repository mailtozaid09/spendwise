import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'


import { useSelector } from 'react-redux';
import { Colors } from '../../global';
import { media } from '../../global/media';
import { Poppins } from '../../global/fontFamily';
import { screenWidth } from '../../global/constants';
import Icon from '../../utils/icons';

import * as Progress from 'react-native-progress';
import { VictoryPie, } from "victory-native";
import LottieView from 'lottie-react-native';



const SummaryScreen = ({navigation}) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    const expense_list = useSelector(state => state.home.expense_list);
    console.log(expense_list);

    useEffect(() => {
        getExpenseData()
    }, [expense_list])
    

    const [pieData, setPieData] = useState([]);
    
    const [grpCategory, setGrpCategory] = useState({});
    const [totalExpense, setTotalExpense] = useState('');


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            console.log('====================================');
            console.log("Summary");
            console.log('====================================');

              
            getExpenseData()
        });
    
        return unsubscribe;
    }, [navigation]);

    const getExpenseData = () => {
        console.log("getTotalExpense");
        getTotalExpense()

        var groupedCategory = groupBy(expense_list, 'categoryName');
        setGrpCategory(groupedCategory)
        createPieData(groupedCategory)
    }


    const createPieData = (groupedCategory) => {
        
        var newArray = Object.entries(groupedCategory)?.map(obj => {
            // console.log('====================================');
            // console.log("obj =>> " , obj);
            // console.log('====================================');
            return { x: obj[0], y: checkAmount(obj[1], obj[0]), };
          });
        setPieData(newArray)
       
    }
    
        
    const getTotalExpense = () => {
        const arr = expense_list

        const total = arr?.reduce((prev,next) => Number(prev) + Number(next.amount),0);
        setTotalExpense(total)
    }

    function groupBy(objectArray, property) {
     
        return objectArray.reduce(function (acc, obj) {
          var key = obj[property];
          if (!acc[key]) {
            acc[key] = [];
          }
          acc[key].push(obj);
          return acc;
        }, {});
    }

    const checkAmount = (array, name) => {
        
        const newArr = array
        const total = newArr?.reduce((prev,next) => Number(prev) + Number(next.amount),0);

        return  total
    }



    return (
        <SafeAreaView style={styles.container} >
            <ScrollView>
                <View style={{ padding: 20,}} >
                {expense_list?.length != 0 && <View style={[styles.categoryItemContainer, {height: 260, marginBottom: 20, alignItems: 'center', justifyContent:'center'}]} >
                    <VictoryPie
                        data={pieData}
                        animate={{
                            duration: 2000
                        }}
                        colorScale={["tomato", "orange", "gold", "cyan", "navy" ]}
                        width={screenWidth-100}
                        style={{
                            data: {
                            },
                            labels: {
                                backgroundColor: 'red',
                                fontSize: 10, textAlign: 'center', fontFamily: Poppins.Medium, fill: Colors[app_theme?.theme_value].white
                            }
                          }}
                    />
                </View>}

            {grpCategory && Object.entries(grpCategory).map((object, objIdx) => {
                return(
                    <View key={objIdx} style={styles.categoryItemContainer} >
                        <View style={[styles.categoryItemIconContainer, {backgroundColor: object[1][0]?.currentCategory?.categoryColor?.bgColor || 'white'}]} >
                            <Icon type={object[1][0]?.currentCategory?.categoryIcon?.type} name={object[1][0]?.currentCategory?.categoryIcon?.title}  size={24} color={object[1][0]?.currentCategory?.categoryColor?.iconColor} />
                        </View>
                        <View style={{flex: 1, flexDirection: 'row', alignItems: 'center'}} >
                            <View style={{flex: 1,  }} >
                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between'}} >
                                    <View style={{flex: 1, width: '100%'}}>
                                        <Text numberOfLines={1} style={styles.categoryItemTitle} >{object[1][0]?.currentCategory?.categoryName}</Text>
                                    </View>
                                    <View style={{ width: 70, alignItems: 'flex-end' }} >
                                        <Text style={[styles.categoryItemTitle, {color: Colors[app_theme?.theme_value].redColor, marginLeft: 4,}]} >-{"\u20B9"} {checkAmount(object[1], object[0])}</Text>
                                    </View>
                                </View>
                                <View style={{marginTop: 4,}} >
                                    <Progress.Bar progress={((checkAmount(object[1], object[0])/totalExpense)).toFixed(1)} width={null} color={Colors[app_theme?.theme_value].primaryColor} />
                                </View>
                                
                            </View>
                            <View style={{width: 60,  marginLeft: 10, alignItems: 'flex-end'}} >
                                <Text style={[styles.categoryItemTitle, {color: Colors[app_theme?.theme_value].primaryColor}]} >{((checkAmount(object[1], object[0])/totalExpense)*100).toFixed(2)}%</Text>
                            </View>
                        </View>
                    </View>
                )
            })}

                
            </View>

            {expense_list?.length == 0 && (
                    <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 20}} >
                    
                            <LottieView
                                source={media.empty_lottie} 
                                autoPlay 
                                loop 
                                style={{height: 200, width: 200, marginBottom: 12}}
                            />
                            <Text style={styles.modalTitle}>Oopss!!!</Text>
                            <Text style={styles.modalSubTitle}>No Expenses added!</Text>
                    </View>
                )}

            </ScrollView>

        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors[theme]?.themeColor,
    },
    maincontainer: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        fontSize: 24,
        margin: 10
    },
    categoryItemContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 15,
        borderColor: 'black',
        backgroundColor: Colors[theme]?.gray,
        padding: 15,
        borderRadius: 8,
        shadowColor: "#000",
        shadowOffset: {
            width: 7,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,
        elevation: 8,
    },
    categoryItemIconContainer: {
        height: 40,
        width: 40,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 4,
    },
    categoryItemTitle: {
        fontSize: 14,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    categoryItemDescription: {
        fontSize: 12,
        fontFamily: Poppins.Regular,
        color: Colors[theme]?.white,
    },
    modalSubTitle: {
        fontSize: 16,
        fontFamily: Poppins.Light,
        color: Colors[theme]?.white,
    },
    modalTitle: {
        fontSize: 24,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    }
})

export default SummaryScreen