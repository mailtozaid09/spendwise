import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import RBSheet from "react-native-raw-bottom-sheet";

import { fontSize, Poppins } from '../../global/fontFamily';

import { media } from '../../global/media';

import PrimaryButton from '../button/PrimaryButton';
import Input from '../input';
import { Colors } from '../../global';
import { FlatList } from 'react-native-gesture-handler';
import Icon from '../../utils/icons';
import { useSelector } from 'react-redux';
import { categories } from '../../global/sampleData';

const FilterSheet = ({refRBSheet, currentCategory, selectCurrentCategory}) => {

    const themes = useColorScheme();
    const styles = styling(themes);

    const category_list = useSelector(state => state.home.category_list);
    console.log("category_list ",category_list);

    const year_options = [
        'All time', '2023', 'Sept', 'This week', 'Today'
    ]

    const payment_options = [
        'All payment modes', 'Online', 'Cash',
    ]


    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={500}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }, 
                    container: {
                        backgroundColor: 'white',
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        paddingTop: 10,
                    }
                }}
            >
                <View style={{}} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20, paddingBottom: 10}} >
                        <Text style={styles.heading} >Select Filters</Text>
                        <TouchableOpacity
                            onPress={() => {refRBSheet.current.close()}}
                        >
                            <Icon type={'AntDesign'} name={'close'}  size={30} color={Colors[themes].white} />
                        </TouchableOpacity>
                        
                    </View>

                    <View style={{padding: 20, paddingTop: 10}} >
                        <Text style={styles.filterHeading}>Year/month</Text>
                        <FlatList
                            data={year_options}
                            horizontal
                            keyExtractor={item => item}
                            renderItem={({item, index}) => (
                                <View key={index} style={{padding: 10, borderWidth: 1, borderRadius: 8, marginRight: 12}} >
                                    <Text>{item}</Text>
                                </View>
                            )}
                        />
                    </View>

                    <View style={{padding: 20, paddingTop: 10}} >
                        <Text style={styles.filterHeading}>Category</Text>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                            <View style={{padding: 10, borderWidth: 1, borderRadius: 8, marginRight: 12}} >
                                <Text>All Categpries</Text>
                            </View>
                            <FlatList
                                data={category_list}
                                horizontal
                                keyExtractor={item => item}
                                renderItem={({item, index}) => (
                                    <View key={index} style={{padding: 10, borderWidth: 1, borderRadius: 8, marginRight: 12}} >
                                        <Text>{item.categoryName}</Text>
                                    </View>
                                )}
                            />
                        </View>
                    </View>

                    <View style={{padding: 20, paddingTop: 10}} >
                        <Text style={styles.filterHeading}>Payment mode</Text>
                        <FlatList
                            data={payment_options}
                            horizontal
                            keyExtractor={item => item}
                            renderItem={({item, index}) => (
                                <View key={index} style={{padding: 10, borderWidth: 1, borderRadius: 8, marginRight: 12}} >
                                    <Text>{item}</Text>
                                </View>
                            )}
                        />
                    </View>

                    



                </View>
            </RBSheet>
        </View>
    )
}

const styling = theme => StyleSheet.create({
    heading: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: Colors[theme].white,
        textAlign: 'center'
    },
    filterHeading: {
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: Colors[theme].white,
    },

    categoryItemIconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        marginTop: 15,
    },

})

export default FilterSheet
