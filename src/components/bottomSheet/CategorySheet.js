import React, { useState, useEffect, useRef } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import RBSheet from "react-native-raw-bottom-sheet";

import { fontSize, Poppins } from '../../global/fontFamily';

import { Colors } from '../../global';

import Icon from '../../utils/icons';
import { useSelector } from 'react-redux';
import { categories } from '../../global/sampleData';

const CategorySheet = ({refRBSheet, currentCategory, selectCurrentCategory}) => {
    
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);
    
    const category_list = useSelector(state => state.home.category_list);
    console.log("category_list ",category_list);


    const otherCategory = {
        categoryName: 'Others',
        categoryDescription: 'Miscellaneous expenses',
        categoryIcon: {"title": "dots-three-horizontal", "type": "Entypo"},
        categoryColor: {
            iconColor: '#015555',
            bgColor: '#ADDCCC',
        },
    }

    return (
        <View>
            <RBSheet
                ref={refRBSheet}
                closeOnDragDown={true}
                closeOnPressMask={true}
                dragFromTopOnly={true}
                height={500}
                onClose={() => {}}
                customStyles={{
                    wrapper: {
                        backgroundColor: "#00000090",
                    },
                    draggableIcon: {
                        backgroundColor: "#000"
                    }, 
                    container: {
                        backgroundColor: Colors[app_theme?.theme_value].themeColor,
                        borderTopRightRadius: 20,
                        borderTopLeftRadius: 20,
                        paddingTop: 10,
                        borderWidth: 1,
                        borderBottomWidth: 0,
                        borderColor: Colors[app_theme?.theme_value].white,
                    }
                }}
            >
                <View style={{backgroundColor: Colors[app_theme?.theme_value].themeColor,}} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: 20, paddingBottom: 10}} >
                        <Text style={styles.heading} >Select Category</Text>
                        <TouchableOpacity
                            onPress={() => {refRBSheet.current.close()}}
                        >
                            <Icon type={'AntDesign'} name={'close'}  size={30} color={Colors[app_theme?.theme_value].white} />
                        </TouchableOpacity>
                        
                    </View>
                    <ScrollView showsVerticalScrollIndicator={true}>
                        <TouchableOpacity activeOpacity={1} style={{padding: 20, paddingTop: 10}} >
                        
                            <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'flex-start', alignItems: 'center'}} >
                                <TouchableOpacity 
                                    
                                    onPress={() => {selectCurrentCategory(otherCategory)}}
                                    style={[styles.categoryItemIconContainer, {backgroundColor: otherCategory?.categoryColor?.bgColor, borderWidth: 1,}, currentCategory?.categoryName == otherCategory?.categoryName ? styles.activeIcon : null,currentCategory?.categoryName == otherCategory?.categoryName ? {borderColor: otherCategory?.categoryColor?.iconColor,} : {borderColor: otherCategory?.categoryColor?.bgColor}  ]} >
                                    <Icon type={otherCategory?.categoryIcon?.type} name={otherCategory?.categoryIcon?.title}  size={32} color={otherCategory?.categoryColor?.iconColor} />
                                </TouchableOpacity>
                                {category_list?.map((item, index) => (
                                    <TouchableOpacity 
                                        key={index}
                                        onPress={() => {selectCurrentCategory(item)}}
                                        style={[styles.categoryItemIconContainer, {backgroundColor: item.categoryColor?.bgColor, borderWidth: 1,}, currentCategory?.categoryName == item.categoryName ? styles.activeIcon : null,currentCategory?.categoryName == item.categoryName ? {borderColor: item.categoryColor?.iconColor,} : {borderColor: item.categoryColor?.bgColor}  ]} >
                                        <Icon type={item.categoryIcon?.type} name={item.categoryIcon?.title}  size={32} color={item.categoryColor?.iconColor} />
                                    </TouchableOpacity>
                                ))}
                            </View> 
                            
                        </TouchableOpacity>
                    </ScrollView>

                  
                </View>
            </RBSheet>
        </View>
    )
}

const styling = theme => StyleSheet.create({
    heading: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: Colors[theme].white,
        textAlign: 'center'
    },
    subheading: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.Medium,
        color: 'gray',
        marginBottom: 15,
        textAlign: 'center'
    },
    categoryContainer: {
        // flexDirection: 'row',
        // alignItems: 'center',
        //justifyContent: 'space-between'
    },
    categoryItems: {
        flexWrap: 'wrap',
        flexDirection: 'row',
        justifyContent: 'space-between'
        //alignItems: 'center',
    },
    saveText: {
        fontSize: fontSize.Title,
        fontFamily: Poppins.Medium,
        color: 'gray',
    },
    label: {
        fontSize: 22,
        fontFamily: Poppins.Light,
        color: 'black',
        marginBottom: 6,
    },


    categoryItemIconContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 15,
        marginTop: 15,
    },
    
    activeIcon: {
     
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 4,
        },
        shadowOpacity: 0.30,
        shadowRadius: 4.65,

        elevation: 15,
    }
})

export default CategorySheet
