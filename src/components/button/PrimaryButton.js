import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, useColorScheme, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';
import { useSelector } from 'react-redux';

const PrimaryButton = ({title, onPress, buttonStyle, textStyle, isDelete, disabled, color }) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(themes);


    return (
        <>
            {isDelete ?
                <TouchableOpacity
                onPress={onPress}
                style={[styles.buttonContainer, buttonStyle, {borderColor: color}]}
                disabled={disabled}
            >
                <Text style={[styles.buttonText, textStyle, {color: color}]} >{title}</Text>
            </TouchableOpacity>
            :
            <TouchableOpacity
                onPress={onPress}
                style={[styles.buttonContainer, buttonStyle, {backgroundColor: Colors[themes].primaryColor}]}
                disabled={disabled}
            >
                <Text style={[styles.buttonText, {color: Colors[themes].commonWhite}]} >{title}</Text>
            </TouchableOpacity>
            }
        </>
    )
}



const styling = theme => StyleSheet.create({
    buttonContainer: {
        height: 60,
        borderWidth: 1,
        borderColor: Colors[theme]?.primaryColor,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        width: screenWidth-40,
    },
    buttonText: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.primaryColor,
    }
})

export default PrimaryButton
