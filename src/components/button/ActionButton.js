import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../global';

import PlusIcon from 'react-native-vector-icons/AntDesign'
import SaveIcon from 'react-native-vector-icons/FontAwesome5'
import { useSelector } from 'react-redux';


const ActionButton = ({title, onPress, type}) => {
       
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);
    
    return (
        <View style={styles.container} >
            <TouchableOpacity 
                activeOpacity={0.5}
                onPress={onPress} 
                style={styles.button} >
                    {type == 'add' 
                    ?
                    <PlusIcon name="plus" size={28} color={Colors[app_theme?.theme_value]?.primaryLightColor} />
                    :
                    <SaveIcon name="save" size={28} color={Colors[app_theme?.theme_value]?.primaryLightColor} />
                    }
                
            </TouchableOpacity>
        </View>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        position: 'absolute', 
        bottom: 20, 
        right: 20,
    },
    button: {
        height: 60, 
        width: 60,
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors[theme]?.primaryColor,
    },
})

export default ActionButton