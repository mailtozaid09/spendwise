import React from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';
import Icon from '../../utils/icons';
import { media } from '../../global/media';
import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';

const HomeHeader = ({}) => {

    const navigation = useNavigation()
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    
    return (
        <View style={styles.container} >
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <TouchableOpacity 
                    onPress={() => {navigation.navigate('ProfileStack')}}
                    style={{flexDirection: 'row', alignItems: 'center'}} >
                    <Image source={media.user_img} style={{height: 50, width: 50, borderRadius: 25, marginRight: 12,}} />
                    <View>
                        <Text style={styles.title} >Hello!</Text>
                        <Text style={styles.subtitle} >Zaid Ahmed</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity 
                    onPress={() => {navigation.navigate('Notification')}}
                    style={{flexDirection: 'row', alignItems: 'center'}} >
                        <Icon type={'FontAwesome'} name={'bell'}  size={26} color={Colors[app_theme?.theme_value].white} />
                    </TouchableOpacity>
            </View>
        </View>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        paddingHorizontal: 20,
        marginBottom: 10,
        paddingTop: 15,
        backgroundColor: Colors[theme]?.themeColor,
    },
    title: {
        fontSize: 14,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.white,
    },
    subtitle: {
        fontSize: 18,
        lineHeight: 20,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
})

export default HomeHeader