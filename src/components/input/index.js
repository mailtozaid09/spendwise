import React, { useState } from 'react';
import { TouchableOpacity, StyleSheet, Dimensions, Text, View, Touchable, TextInput, Image, useColorScheme, } from 'react-native';

import { screenWidth } from '../../global/constants';
import { fontSize, Poppins } from '../../global/fontFamily';
import { media } from '../../global/media';
import { Colors } from '../../global';

const Input = ({ placeholder, isSearch, label, value,onClearSearchText, onChangeText, onChangeEyeIcon, isPassword, showEyeIcon }) => {
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    return (
        <View>

        <Text style={styles.label} >{label}</Text>

        <View style={[styles.inputContainer]}  >

            {isSearch && (
                <TouchableOpacity disabled={true} onPress={onChangeEyeIcon}>
                    <Image source={media.account} style={[styles.iconImage, {marginRight: 15}]} />
                </TouchableOpacity>
            )}

            <TextInput
                placeholder={placeholder}
                style={styles.inputStyle}
                value={value}
                onChangeText={onChangeText}
                placeholderTextColor={Colors[app_theme?.theme_value]?.black}
                secureTextEntry={isPassword  && !showEyeIcon ? true : false}
            />

            {isPassword && (
                <TouchableOpacity onPress={onChangeEyeIcon}>
                    <Image source={showEyeIcon ? media.account : media.account} style={styles.iconImage} />
                </TouchableOpacity>
            )}

            {isSearch && value && (
                <TouchableOpacity onPress={onClearSearchText}>
                    <Image source={media.account} style={[styles.iconImage, {position: 'absolute', right: -5}]} />
                </TouchableOpacity>
            )}
        </View>
        </View>
    )
}


const styling = theme => StyleSheet.create({
    inputContainer: {
        // height: 60,
        paddingHorizontal: 15,
        paddingVertical: 4,
        borderWidth: 1,
        borderColor: Colors[theme]?.black,
        borderRadius: 10,
        width: screenWidth-40,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    inputStyle: {
        fontSize: fontSize.SubTitle,
        fontFamily: Poppins.Medium,
        color: 'black',
        width: screenWidth-120,
    },
    iconImage: {
        height: 24,
        width: 24
    },
    label: {
        fontSize: fontSize.Body,
        fontFamily: Poppins.SemiBold,
        color: 'black',
        marginBottom: 6,
    }
})

export default Input
