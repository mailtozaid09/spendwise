import React, {useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Alert, Pressable, Modal, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';

import LottieView from 'lottie-react-native';
import { media } from '../../global/media';
import { useSelector } from 'react-redux';

const AlertModal = ({closeModal, title, description, alertType, onCancel, onAdd, onDelete, onEdit, onLogout}) => {
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);
    
    const [modalVisible, setModalVisible] = useState(true);

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Are you sure! \nYou wan to close the modal?');
                        closeModal()
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            
                            <View>
                                <LottieView 
                                    source={
                                        alertType == 'Add' ? media.success_lottie
                                        : alertType == 'Edit' ? media.edit_lottie
                                        : alertType == 'Delete' ? media.error_lottie
                                        : alertType == 'Logout' ? media.logout_lottie
                                        : media.success_lottie
                                        } 
                                    autoPlay 
                                    loop 
                                    style={{height: 100, width: 100, marginBottom: 12}}
                                />
                            </View>

                            <Text style={[styles.modalTitle, {color: alertType == 'Delete' || alertType == 'Logout' ? Colors[app_theme?.theme_value].redColor : Colors[app_theme?.theme_value].greenColor}]}>{title}</Text>
                            
                            <Text style={styles.modalDescription}>{description}</Text>

                                {alertType == 'Edit' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: Colors[app_theme?.theme_value].commonWhite}]}
                                            onPress={onCancel}>
                                            <Text style={[styles.textStyle, {color: Colors[app_theme?.theme_value].primaryColor}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonEdit]}
                                            onPress={onEdit}>
                                            <Text style={styles.textStyle}>Edit</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Delete' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: Colors[app_theme?.theme_value].primaryColor}]}
                                            onPress={onCancel}>
                                            <Text style={[styles.textStyle, {color: Colors[app_theme?.theme_value].primaryLightColor}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonDelete, {borderWidth: 1, backgroundColor: Colors[app_theme?.theme_value].commonWhite, borderColor: Colors[app_theme?.theme_value].redColor}]}
                                            onPress={onDelete}>
                                            <Text style={[styles.textStyle, {color: Colors[app_theme?.theme_value].redColor}]}>Delete</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Logout' && (
                                    <View style={{flexDirection: 'row', width: '100%', alignItems: 'center', justifyContent: 'space-between'}} >
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonClose, {backgroundColor: Colors[app_theme?.theme_value].primaryColor}]}
                                            onPress={closeModal}>
                                            <Text style={[styles.textStyle, {color: Colors[app_theme?.theme_value].primaryLightColor}]}>Cancel</Text>
                                        </TouchableOpacity> 
                                        <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonDelete, {borderWidth: 1, backgroundColor: Colors[app_theme?.theme_value].commonWhite, borderColor: Colors[app_theme?.theme_value].redColor}]}
                                            onPress={onLogout}>
                                            <Text style={[styles.textStyle, {color: Colors[app_theme?.theme_value].redColor}]}>Logout</Text>
                                        </TouchableOpacity> 
                                    </View>
                                )}

                                {alertType == 'Add' && (
                                    <View style={{}} >
                                         <TouchableOpacity
                                            activeOpacity={0.5}
                                            style={[styles.button, styles.buttonAdd]}
                                            onPress={onAdd}>
                                            <Text style={styles.textStyle}>Okay</Text>
                                        </TouchableOpacity>
                                    </View>
                                )}
                        </View>
                    </View>
                </Modal>
            </View>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: Colors[theme]?.themeColorOpac,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
      },
    modalView: {
        margin: 20,
        width: '80%',
        backgroundColor: Colors[theme]?.themeColor,
        borderRadius: 20,
        padding: 25,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
      },
    button: {
        borderRadius: 30,
        height: 50,
        width: 100,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    buttonAdd: {
        backgroundColor: Colors[theme]?.primaryColor,
    },
    buttonEdit: {
        backgroundColor: Colors[theme]?.primaryColor,
    },
    buttonDelete: {

    },
    buttonClose: {
        borderWidth: 1,
        backgroundColor: Colors[theme]?.gray,
        borderColor: Colors[theme]?.primaryColor,
    },
    textStyle: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.primaryLightColor,
    },
    modalTitle: {
        textAlign: 'center',
        fontSize: 20,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    modalDescription: {
        marginBottom: 15,
        textAlign: 'center',
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
})

export default AlertModal