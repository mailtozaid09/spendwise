import React, {useCallback, useEffect, useState} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, Alert, Pressable, Modal, } from 'react-native'

import { Colors } from '../../global';
import { Poppins } from '../../global/fontFamily';

import LottieView from 'lottie-react-native';
import { media } from '../../global/media';
import { get, save } from '../../utils/storage';
import { useSelector } from 'react-redux';

const ThemeModal = ({closeModal, currentTheme,themeOperations, onChangeTheme}) => {
    
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    
    
    const [modalVisible, setModalVisible] = useState(true);

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.centeredView}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Are you sure! \nYou wan to close the modal?');
                        closeModal()
                    }}>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            
                            <View>
                                <LottieView 
                                    source={media.dark_mode_lottie} 
                                    autoPlay 
                                    loop 
                                    style={{height: 100, width: 150, marginBottom: 0}}
                                />
                            </View>

                            <TouchableOpacity 
                                onPress={() => {closeModal(); onChangeTheme('light'); themeOperations('light')}}
                                style={[styles.modalContainer, app_theme?.theme_type == 'light_theme' ? { backgroundColor: Colors[themes].primaryColor,  } : null]} >
                                <Text style={[styles.modalTitle, app_theme?.theme_type == 'light_theme' ? {color: Colors[themes].primaryLightColor,} : null]} >Always Light</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => {closeModal(); onChangeTheme('dark'); themeOperations('dark')}}
                                style={[styles.modalContainer, app_theme?.theme_type == 'dark_theme' ? { backgroundColor: Colors[themes].primaryColor, } : null]} >
                                <Text style={[styles.modalTitle, app_theme?.theme_type == 'dark_theme' ? {color: Colors[themes].primaryLightColor,} : null]} >Always Dark</Text>
                            </TouchableOpacity>

                            <TouchableOpacity 
                                onPress={() => {closeModal(); onChangeTheme('default'); themeOperations('default')}}
                                style={[styles.modalContainer, app_theme?.theme_type == 'default_theme' ? { backgroundColor: Colors[themes].primaryColor, } : null]} >
                                <Text style={[styles.modalTitle, app_theme?.theme_type == 'default_theme' ? {color: Colors[themes].primaryLightColor,} : null]} >System</Text>
                            </TouchableOpacity>

                        </View>
                    </View>
                </Modal>
            </View>
        </SafeAreaView>
    )
}

const styling = theme => StyleSheet.create({
    container: {
        flex: 1,
        height: '100%',
        width: '100%',
        position: 'absolute',
        backgroundColor: Colors[theme]?.themeColorOpac,
    },
    centeredView: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 22,
      },
    modalView: {
        margin: 20,
        width: '80%',
        backgroundColor: Colors[theme]?.themeColor,
        borderRadius: 20,
        padding: 25,
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: {
          width: 0,
          height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5,
      },
    button: {
        borderRadius: 30,
        height: 50,
        width: 100,
        elevation: 2,
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    buttonAdd: {
        backgroundColor: Colors[theme]?.primaryColor,
    },
    buttonEdit: {
        backgroundColor: Colors[theme]?.primaryColor,
    },
    buttonDelete: {

    },
    buttonClose: {
        borderWidth: 1,
        backgroundColor: Colors[theme]?.gray,
        borderColor: Colors[theme]?.primaryColor,
    },
    textStyle: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: Colors[theme]?.primaryLightColor,
    },
    modalTitle: {
        textAlign: 'center',
        fontSize: 16,
        fontFamily: Poppins.Medium,
        color:  Colors[theme]?.primaryColor,
    },
    modalContainer: {
        height: 50,
        marginTop: 10,
        backgroundColor: Colors[theme]?.primaryLightColor,
        width: '100%',
        borderRadius: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },

})

export default ThemeModal