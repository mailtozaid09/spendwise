import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import WelcomeScreen from '../../screens/auth/welcome';
import { Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';

const Stack = createStackNavigator();


const LoginStack = ({navgation}) => {

    const themes = useColorScheme();
    const styles = styling(themes);

    return (
        <Stack.Navigator 
            initialRouteName="Welcome" 
        >
            <Stack.Screen
                name="Welcome"
                component={WelcomeScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    headerTitle: 'Welcome',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            {/* <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Login',
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
           */}
            

        </Stack.Navigator>
    );
}

const styling = theme => StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    headerStyle: {
        backgroundColor: Colors[theme]?.themeColor,
    }
})


export default LoginStack