import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import CategoryScreen from '../../screens/category';
import AddCategoryScreen from '../../screens/category/AddCategory';
import EditCategoryScreen from '../../screens/category/EditCategory';
import { Poppins, fontSize } from '../../global/fontFamily';
import { Colors } from '../../global';
import { useSelector } from 'react-redux';



const Stack = createStackNavigator();

const CategoryStack = ({navgation}) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    return (
        <Stack.Navigator 
            initialRouteName="Category" 
        >
            <Stack.Screen
                name="Category"
                component={CategoryScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Category',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddCategory"
                component={AddCategoryScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    headerTintColor: Colors[app_theme?.theme_value]?.white,
                    headerLeftLabelVisible: false,
                    headerTitle: 'Add Category',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="EditCategory"
                component={EditCategoryScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    headerTintColor: Colors[app_theme?.theme_value]?.white,
                    headerTitle: 'Edit Category',
                    headerLeftLabelVisible: false,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styling = theme => StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    headerStyle: {
        backgroundColor: Colors[theme]?.themeColor,
    }
})

export default CategoryStack