import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform, useColorScheme,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { media } from '../../global/media';

import AsyncStorage from '@react-native-async-storage/async-storage';

import HomeStack from '../home';
import ProfileStack from '../profile';
import SummaryStack from '../summary';
import CategoryStack from '../category';
import { Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';
import AddButtonScreen from '../../screens/home/AddButton';
import Icon from '../../utils/icons';
import { useSelector } from 'react-redux';

const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {
    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    const [userDetails, setUserDetails] = useState({});

    useEffect(() => {
        //getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)
    }

    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 24, width = 24 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.home_active : media.home
                    routeName = 'Home' 
                } 
                else if (route.name === 'SummaryStack') {
                    iconName = focused ? media.summary_active : media.summary
                    routeName = 'Summary' 
                }
                else if (route.name === 'CategoryStack') {
                    iconName = focused ? media.category_active : media.category
                    routeName = 'Category' 
                }
                else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.user_active : media.user
                    routeName = 'Profile' 
                    height = 22, width = 22 ;
                }
                return(
                    <View style={[{alignItems: 'center', justifyContent: 'center', height: 44, width: 44, borderRadius: 22, flexDirection: 'row' },  ]} >
                        <Image source={iconName} style={{height: height, width: width, }}/>
                        {/* {focused && <Text style={{fontSize: 10, marginLeft: 4, color: Colors[themes]?.primaryColor, fontFamily: Poppins.Bold}} >{routeName}</Text>} */}
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    headerRight: () => null,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddExpense' || routeName === 'EditExpense') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

            
            <Tab.Screen
                name="SummaryStack"
                component={SummaryStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            
            {/* <Tab.Screen
                name='AddButtonScreen'
                component={AddButtonScreen}
                options={({navigation, route}) => ({
                    tabBarIcon: ({focused}) => (
                        <Icon type={'AntDesign'} name={'plus'}  size={30} color={Colors[themes].commonWhite} />
                    ),
                    tabBarButton: (props) => (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            onPress={() => {navigation.navigate('AddExpense') }}
                            style={{
                                top: -30,
                                alignItems: 'center',
                                justifyContent: 'center',
                            }}
                        >
                            <View
                                style={{
                                    height: 50,
                                    width: 50, 
                                    borderRadius: 25,
                                    borderWidth: 0.5,
                                    borderColor: Colors[themes].primaryColor,
                                    backgroundColor: Colors[themes].primaryLightColor,
                                    alignItems: 'center',
                                    justifyContent: 'center',
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 6,
                                    },
                                    shadowOpacity: 0.37,
                                    shadowRadius: 7.49,

                                    elevation: 6,
                                }}
                            >
                               <Icon type='AntDesign' name={'plus'}  size={35} color={Colors[themes].primaryColor} />
                            </View> 
                        </TouchableOpacity>
                    ),
                })}
            />
 */}

            <Tab.Screen
                name="CategoryStack"
                component={CategoryStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory' || routeName === 'EditCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCategory') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
        </Tab.Navigator>
    );
}



const styling = theme => StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 0.4,
        borderColor: Colors[theme]?.themeColor,
        backgroundColor: Colors[theme]?.themeColor,
    }
})