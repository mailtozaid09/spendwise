import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../../screens/home';
import AddExpenseScreen from '../../screens/home/AddExpense';
import AddButtonScreen from '../../screens/home/AddButton';
import { Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';
import EditExpenseScreen from '../../screens/home/EditExpense';
import NotificationScreen from '../../screens/home/Notification';
import { useSelector } from 'react-redux';


const Stack = createStackNavigator();

const HomeStack = ({navgation}) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    headerLeft: () => null,
                    headerTitle: 'Home',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="AddExpense"
                component={AddExpenseScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    headerTintColor: Colors[app_theme?.theme_value]?.white,
                    headerTitle: 'Add Expense',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="EditExpense"
                component={EditExpenseScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    headerTintColor: Colors[app_theme?.theme_value]?.white,
                    headerTitle: 'Edit Expense',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            <Stack.Screen
                name="Notification"
                component={NotificationScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    headerTitle: 'Notifications',
                    headerTintColor: Colors[app_theme?.theme_value]?.white,
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            
            <Stack.Screen
                name="AddButtonScreen"
                component={AddButtonScreen}
                options={{
                    headerShown: true,
                    // headerLeft: () => null,
                    // headerTitle: 'Cart',
                    // headerTitleStyle: styles.headerTitleStyle,
                }}
            />
            
        </Stack.Navigator>
    );
}

const styling = theme => StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    headerStyle: {
        backgroundColor: Colors[theme]?.themeColor,
    }
})

export default HomeStack