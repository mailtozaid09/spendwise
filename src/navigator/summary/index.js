import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';

import SummaryScreen from '../../screens/summary';
import { Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';
import { useSelector } from 'react-redux';


const Stack = createStackNavigator();

const SummaryStack = ({navgation}) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);


    return (
        <Stack.Navigator 
            initialRouteName="Summary" 
        >
            <Stack.Screen
                name="Summary"
                component={SummaryScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Summary',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styling = theme => StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    headerStyle: {
        backgroundColor: Colors[theme]?.themeColor,
    }
})

export default SummaryStack