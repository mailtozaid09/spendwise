import React, {useState, useEffect} from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import AsyncStorage from '@react-native-async-storage/async-storage';


import Tabbar from './tabbar';
import LoginStack from './login';

import { useNavigation } from '@react-navigation/native';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();


const Navigator = ({}) => {

    const navigation = useNavigation()

    const [userDetails, setUserDetails] = useState(null);

    const user_logged_in = useSelector(state => state.auth.user_logged_in);

    useEffect(() => {
        getUserDetails()
    }, [])
    

    const getUserDetails = async () => {
        let user = await AsyncStorage.getItem('user_details');  
        let user_details = JSON.parse(user);  
        setUserDetails(user_details)

    }
   


    return (
        <>
        <Stack.Navigator 
        initialRouteName={user_logged_in ? 'Tabbar' : 'LoginStack'}  
        >
            <Stack.Screen
                name="LoginStack"
                component={LoginStack}
                options={{
                    headerShown: false
                }}
            />
            <Stack.Screen
                name="Tabbar"
                component={Tabbar}
                options={{
                    headerShown: false
                }}
            />
        </Stack.Navigator>
        
        </>
    );
}

export default Navigator