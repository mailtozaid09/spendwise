import React from 'react';
import { SafeAreaView, Text, View, Button, TouchableOpacity, Image, StyleSheet, Dimensions, Platform, ScrollView, KeyboardAvoidingView, useColorScheme, } from 'react-native';

import { createStackNavigator } from '@react-navigation/stack';
import ProfileScreen from '../../screens/profile';
import { Poppins } from '../../global/fontFamily';
import { Colors } from '../../global';
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();

const ProfileStack = ({navgation}) => {

    const app_theme = useSelector(state => state.profile.app_theme);
    const themes = useColorScheme();
    const styles = styling(app_theme?.theme_value);

    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: true,
                    headerLeft: () => null,
                    headerTitle: 'Profile',
                    headerStyle: styles.headerStyle,
                    headerTitleStyle: styles.headerTitleStyle,
                }}
            />
        </Stack.Navigator>
    );
}

const styling = theme => StyleSheet.create({
    headerTitleStyle: {
        fontSize: 18, 
        fontFamily: Poppins.Medium,
        color: Colors[theme]?.white,
    },
    headerStyle: {
        backgroundColor: Colors[theme]?.themeColor,
    }
})

export default ProfileStack